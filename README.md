# AWS Fullstack Starter Kit (AFSK)

The AWS Fullstack Starter Kit is a comprehensive collection of GitLab projects designed to kickstart development on AWS. Featuring a React-based front-end and complete CI/CD pipelines, this kit simplifies the process of provisioning and deploying the necessary infrastructure for a basic AWS application.

Originally adapted from a personal project, the AFSK is intended to demonstrate a wide range of technical skills for potential employers. It's also structured to be beneficial for individual developers looking to leverage AWS for their applications.

### Repositories Overview

- `afsk-pipelines`: Contains the GitLab CI/CD pipeline configurations and resources.
- `afsk-vpc`: Sets up the AWS Virtual Private Cloud (VPC) and other necessary resources for backend services.
- `afsk-user-service`: A RESTful Node.js service for user management and authentication.
- `afsk-backend-service`: A scalable RESTful Node.js service template to customize for specific backend needs.
- `afsk-react-ui`: A React-based user interface utilizing the `create-react-app` framework for quick setup and development.

## Architectural Overview

Below is a simplified architectural diagram illustrating the interaction between the project's components. Please note, for simplicity, details like security groups and network configurations are omitted.

![Simplified Architectural Diagram](overview.svg)

## Getting Started

### Prerequisites for Local Development

Ensure the following tools are installed and properly configured:

- AWS CLI
- Docker
- Git
- NodeJS (v20 or later)
- NPM
- Terraform

#### Docker Network Setup

After installing Docker, initialize the required network for local development containers with the following command:

```bash
docker network create --driver bridge afsk_network
```

### Configuring GitLab CI/CD

The AFSK utilizes GitLab CI/CD pipelines across all repositories. To replicate this setup:

1. **Create a GitLab Group**: Organize the repositories within a GitLab group similar to this project's structure.

2. **Generate a `GITLAB_CI_TOKEN`**:
   - Navigate to your GitLab group settings, then to Access Tokens.
   - Create a token with `read_repository` and `write_repository` permissions.
   - Note down the token value for later use.

3. **AWS User Setup for Pipelines**:
   - Deploy the `aws-pipeline-user.yaml` from `afsk-pipelines` using AWS CloudFormation to create a user with the necessary permissions. Alternatively, set up a custom user according to your security preferences.
   - Adjust the shared variables in the pipelines as necessary to match your AWS user setup.

4. **Configure GitLab CI/CD Variables**:
   Go to your GitLab group's CI/CD settings and add the following variables:
   - `AWS_ACCESS_KEY_ID`
   - `AWS_ACCOUNT_ID`
   - `AWS_DEFAULT_REGION`
   - `AWS_SECRET_ACCESS_KEY`
   - `GITLAB_CI_TOKEN`
   - `TF_VAR_jwt_secret_key` (used in `afsk-user-service` for JWT tokens)

5. **Build and Push Docker Images**:
   For each required Docker image, build and push it to your GitLab container registry using the following template (replace paths and tags as needed):

```bash
docker build -t registry.gitlab.com/<your-group>/<your-repo>/afsk-deploy:<tag> -f afsk-deploy.Dockerfile .
docker push registry.gitlab.com/<your-group>/<your-repo>/afsk-deploy:<tag>
```

### Deployment
The order of repo deployment is important in the initial creation of project resources.
After performing all the steps in the sections above along with any other prerequisites
for each repository in this group, deploy in the following order:
- `afsk-vpc`
- `afsk-user-service`
- `afsk-backend-service`
- `afsk-react-ui`

## Important Reminders for Development

As you embark on leveraging this GitLab group for your application development,I want to remind you of several key practices that will help ensure the success and security of your projects:

### Emphasize Testing

- **Unit Tests & Functional Tests:** Rigorous testing is crucial. I strongly recommend implementing comprehensive unit and functional tests for the `user-service`, `backend-service`, and any other services your application relies on. Testing early and often ensures code quality, functionality, and helps catch issues before they become problems.

### Customization and Adaptation

- **Modifying Components:** This GitLab group provides a foundational setup for developing your infrastructure and application. It is expected and encouraged that you will modify components as necessary to fit your specific needs. Consider this group a starting point; your creativity and adjustments will bring the project to life.

### Authentication and Security

- **Strengthening Authentication:** While JWT (JSON Web Tokens) provides a solid starting point for user authentication, I encourage exploring and implementing stronger authentication mechanisms to enhance security. Subsequently, ensure any necessary changes are also made to the frontend to support these enhancements.

### Additional Suggestions

- **Security Practices:** Always prioritize security in your development process. This includes regular dependency updates, adhering to secure coding practices, and conducting security audits.
- **Performance Optimization:** Continuously monitor and optimize the performance of your services. Utilize caching, optimize database queries, and consider using a CDN for static assets to enhance your application's responsiveness and speed.
- **Monitoring and Logging:** Implement comprehensive logging and monitoring solutions to keep track of your application's health and performance. This will aid in quick debugging and informed decision-making.

By keeping these guidelines in mind, you'll be well on your way to creating a robust, secure, and high-quality application. Remember, the journey of development is continuous, and there's always room for improvement and innovation.
